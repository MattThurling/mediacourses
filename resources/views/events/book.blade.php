@extends('layouts.app')

@section('title', 'Book')

@section('content')
	<div class="container">
		<h1>Book a place</h1>
		<p>{{ $event['course']->title }} in {{ $event['venue']->city }} </p>
		<p>{{ $event->starts->format('l d') }} to {{ $event->ends->format('l d F Y') }}  </p>
		<hr />

		{!! Form::open(['url' => 'booking-requests']) !!}

			{!! Form::hidden('event_id', $event->id) !!}

			<div class="form-group">
				{!! Form::label('first_name', 'First name:') !!}
				{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('last_name', 'Last name:') !!}
				{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('email', 'Email:') !!}
				{!! Form::text('email',null, ['class' => 'form-control']) !!}
			</div>
			<hr />



			<div class="form-group">
				{!! Form::submit('Submit booking request', ['class' => 'btn btn-primary form-control']) !!}

		{!! Form::close() !!}

		@if ($errors->any())
			<ul class="alert alert-danger">
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		@endif
	</div>

@endsection