@extends('layouts.app')

@section('title', 'Courses')

@section('content')
	<div class="container">
		@foreach ($courses as $course)
			<h1><a href='{{ route("courses.show", $course->slug) }}'>{{ ($course->title) }}</a></h1>
			<p>{{ ($course->summary) }}</p>
		@endforeach
	</div>
@endsection



