@extends('layouts.app')

@section('content')

        <div class="jumbotron">
          <div class="container">
            <h1>Media Courses</h1>
            <h3>Shortcuts to a great career</h3>
            <p><a href="courses" class="btn btn-primary btn-lg" role="button">View All Courses</a></p>
          </div>
        </div>

        <div id="whatis" class="content-section-b" style="border-top: 0">
          <div class="container">

             <div class="col-md-6 col-md-offset-3 text-center wrap_title">
              <h2>Training Courses</h2>
              <p style="margin-top:0">Short courses running at our training centres in London and Bristol.</p>
              
            </div>

            <div class="row"><!-- first row -->

              <div class="course-panel col-sm-4 text-center">
                <img src="{{ URL::asset('img/thumbs/pp.jpg') }}">
                <h3>Adobe Premiere Pro</h3>

                <p><a href="{!! url('courses/tags/premiere') !!}" class="btn btn-info" role="button">View Courses</a></p>
              </div>

               <div class="course-panel col-sm-4 text-center">
                <img src="{{ URL::asset('img/thumbs/fcpx.jpg') }}">
                <h3>Final Cut Pro X</h3>

                <p><a href="{!! url('courses/tags/final-cut-pro') !!}" class="btn btn-info" role="button">View Courses</a></p>
              </div>

               <div class="course-panel col-sm-4 text-center">
                <img src="{{ URL::asset('img/thumbs/ae.jpg') }}">
                <h3>Adobe After Effects</h3>

                <p><a a href="{!! url('courses/tags/after-effects') !!}" class="btn btn-info" role="button">View Courses</a></p>
              </div>

            </div>

          

          </div>
        </div>

@endsection
