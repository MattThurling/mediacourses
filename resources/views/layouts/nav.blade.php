<nav class='navbar navbar-inverse' role='navigation'>
    <div class='container'>

        <div class='navbar-header'>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                Media Courses
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <?php if ( ! isset($activeNav)) $activeNav = ''; ?>
                <li @if ($activeNav == 'video-production') class="active" @endif><a href="{!! url('courses/tags/video-production') !!}">Video Production</a></li>
                <li @if ($activeNav == 'graphic-design') class="active" @endif><a href="{!! url('courses/tags/graphic-design') !!}">Graphic Design</a></li>
                <li @if ($activeNav == 'web-development') class="active" @endif><a href="{!! url('courses/tags/web-development') !!}">Web Development</a></li>
                <li @if ($activeNav == 'web-development') class="active" @endif><a href="{!! url('courses/tags/adobe') !!}">Adobe</a></li>
                <li @if ($activeNav == 'web-development') class="active" @endif><a href="{!! url('courses/tags/microsoft') !!}">Microsoft</a></li>
                <li @if ($activeNav == 'web-development') class="active" @endif><a href="{!! url('courses/tags/apple') !!}">Apple</a></li>
                <li @if ($activeNav == 'web-development') class="active" @endif><a href="{!! url('courses/tags/linux') !!}">Linux</a></li>

            </ul>

        </div>

    </div>
</nav>