<?php

namespace App\Eloquent;

trait Sluggable
{
    /**
     * Scope to entities with the given slug.
     *
     * @param Builder $query
     * @param string $slug
     */
    public function scopeSlug($query, $slug)
    {
        $query->where('slug', $slug);
    }
}