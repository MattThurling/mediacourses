<?php

namespace App\Http\Controllers;

use Mail;
use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
	public function index ()
	{
    	$events = Event::with('course','venue')->get();
    	return view('events.index', compact('events'));
    }
    public function book ($e)
    {
    	$event = Event::where('id', '=', $e)->with('course','venue')->first();
    	// having to pass the value directly because having trouble extracting it in Blade

    	return view('events.book', compact('event'), compact('e'));
    }
}
