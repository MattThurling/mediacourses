<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Eloquent\Sluggable;
use \Conner\Tagging\Taggable;


class Course extends Model
{	
	use Taggable;
	use Sluggable;
    //
    public function events ()
    {
    	return $this->hasMany(Event::class);
    }

	    /**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return 'slug';
	}
	
}
