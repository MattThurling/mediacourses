<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use App\Course;
use App\Event;
use App\Venue;

class FeedController extends Controller
{
	public function feedEvents ()
	{
        $client = new Client(); //GuzzleHttp\Client
        // $result = $client->get('http://transmedia.co.uk/office/public-data/bfa/');
        // dd($result); 
        // Send an asynchronous request.
        $request = new \GuzzleHttp\Psr7\Request('GET', 'http://transmedia.co.uk/office/public-data/bfa/');
        $promise = $client->sendAsync($request)->then(function ($response) {
            echo 'Got what I need! <br/>';
            $tmevents = json_decode($response->getBody());
            //Clear the exiting events list
            Event::truncate();
            //Repopulate with the new data
            foreach ($tmevents as $tmevent) {
                $course = Course::where('ref', '=', $tmevent->CourseID)->first();
                $venue = Venue::where('ref', '=', $tmevent->Venue)->first();
                if ($course) {
                    $event = new Event;
                    $event->course_id = $course->id;
                    $event->venue_id = $venue->id;
                    $event->starts = $tmevent->StartDateOfCourse;
                    $event->ends = $tmevent->EndDateOfCourse;
                    $event->price = ($tmevent->Price) * 100;
                    $event->save();
                    echo ('Saved a new event: ' . $tmevent->CourseName . ' on ' . $tmevent->StartDateOfCourse . ' in ' . $tmevent->Venue . '<br/>');
                };
            };

        });
        $promise->wait();

        }	

    public function feedCourses ()
    {
        $client = new Client(); //GuzzleHttp\Client
        // $result = $client->get('http://transmedia.co.uk/office/public-data/bfa/');
        // dd($result); 
        // Send an asynchronous request.
        $request = new \GuzzleHttp\Psr7\Request('GET', 'http://transmedia.co.uk/office/public-data/bfa/courses/');
        $promise = $client->sendAsync($request)->then(function ($response) {
            echo 'Got what I need! <br/>';
            $tmcourses = json_decode($response->getBody());

            //Populate with the new data
            foreach ($tmcourses as $tmcourse) {
                //First, check whether the course exists
                $course = Course::where('ref', '=', $tmcourse->CourseID)->first();
                //If not, then new one up
                if (!$course) {
                    $course = new Course;
                };
                $course->title = $tmcourse->CourseName;
                $course->summary = $tmcourse->Objectives;
                $course->content = $tmcourse->CourseDetails;
                $course->ref = $tmcourse->CourseID;
                $course->slug = str_slug($tmcourse->CourseName , '-');
                $course->save();
                echo ('Saved a course: ' . str_slug($tmcourse->CourseName , '-'). '<br/>');
            };
        });
        $promise->wait();

        }   

        public function tagCourses ()
        {
            $tagmap =
            [
            ['Adobe','Adobe'],
            ['Apple', 'Apple'],
            ['SQL','Programming'],
            ['After Effects','After Effects'],
            ['After Effects','Motion graphics'],
            ['After Effects','Video production'],
            ['Dreamweaver','Web development'],
            ['Illustrator','Graphic design'],
            ['Illustrator','Illustrator'],
            ['Photoshop','Photoshop'],
            ['Photoshop', 'Graphic design'],
            ['InDesign', 'Indesign'],
            ['InDesign', 'Graphic design'],
            ['Audition', 'Audio'],
            ['Captivate','eLearning'],
            ['Lightroom','Graphic design'],
            ['Premiere','Premiere'],
            ['Premiere','Video editing'],
            ['Premiere','Video production'],
            ['OS X','Apple'],
            ['macOS','Apple'],
            ['iOS','Apple'],
            ['Final Cut', 'Final Cut Pro'],
            ['Final Cut', 'Apple'],
            ['Final Cut', 'Video editing'],
            ['Final Cut', 'Video production'],
            ['Motion', 'Apple'],
            ['Motion', 'Motion graphics'],
            ['Motion','Video production'],
            ['Logic Pro','Apple'],
            ['Logic Pro', 'Audio'],
            ['Swift', 'Programming'],
            ['iPad','Apple'],
            ['iMovie','Video editing'],
            ['iMovie','Video production'],
            ['Speedgrade','Video editing'],
            ['Speedgrade','Video production'],
            ['iBooks','Apple'],
            ['SQL','SQL'],
            ['Microsoft','Microsoft'],
            ['Sharepoint','Sharepoint'],
            ['Filemaker','Filemaker'],
            ['Javascript','Web development'],
            ['Javascript','Programming'],
            ['HTML','Web development'],
            ['HTML','Programming'],
            ['Web','Wed development'],
            ['Design','Graphic design'],
            ['Video','Video production'],
            ['UX','Web development'],
            ['SEO','Digital marketing'],
            ['Writing','Copywriting'],
            ['Writing','Digital marketing'],
            ['Social Media','Digital marketing'],
            ['Proofreading','Copywriting'],
            ['Photography','Photography'],
            ['Photoshop','Photography'],
            ['Lightroom','Photography'],
            ['Programming','Programming'],
            ['Colour Grading','Video editing'],
            ['Colour Grading','Video production'],
            ['PHP','PHP'],
            ['PHP','Web development'],
            ['PHP','Programming'],
            ['C++','C++'],
            ['C++','Programming'],
            ['4D','3D'],
            ['SketchUp','3D'],
            ['Wordpress','Web development'],
            ['Wordpress','Digital marketing'],
            ['Wordpress','Wordpress'],
            ['Analytics','Digital marketing'],
            ['Articulate','eLearning'],
            ['Davinci','Video production'],
            ['Davinci','Video editing'],
            ['Pro Tools','Audio'],
            ['Oracle','Database'],
            ['SQL','Database'],
            ['Filemaker','Database'],
            ['Oracle','Programming'],
            ['Audacity','Audio'],
            ['Linux','Linux']
            ];
            
            foreach (Course::all() as $course)
            {
                foreach ($tagmap as $tag)
                {
                    if  (str_contains($course->title , $tag[0]))
                    {
                        //check the course doesn't already have this tag
                        //First, get an array of all the tags for this course
                        $alreadyTags = $course->tags;
                        //Set a flag to zero
                        $tagFlag = 0;
                            //Cycle through the tags
                            foreach ($alreadyTags as $alreadyTag)
                            {
                                //and check if the name of this tag is the same as the one we want to add
                                if ($alreadyTag->name == $tag[1])
                                {
                                    echo ($course->title . ' is already tagged: ' . $tag[1] . '<br/>');
                                    //set the flag to true
                                    $tagFlag = 1;
                                }                        
                            }
                        //After going through cycle, check the flag not raised
                        if (!$tagFlag) {
                            //Add the new tag
                            $course->tag($tag[1]);
                            echo ($course->title . ' newly tagged: ' . $tag[1] . '<br/>');
                        }
                    };

                };
            }
            
        }
}
