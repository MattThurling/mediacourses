<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use \Conner\Tagging\Taggable;


class CourseController extends Controller
{
    //
     public function index()
    {
    	$courses = Course::with('events')->get();
    	return view('courses.index', compact('courses'));
    }


    public function show($c)
    {
        $course = Course::where('slug', '=', $c)->first();
        $events = $course->events->load('venue');
        return view('courses.show', compact('course'), compact('events'));
    }

    public function showTagged ($t)
    {
        $courses = Course::withAnyTag($t)->get();
        //Really ugly way of getting the tag name!
        $tags = $courses->first()->tags;
        $tag = $tags->where('slug','=',$t)->first();
        $tagname = $tag['name'];
        return view('courses.tags', compact('courses'), compact('tagname'));        
    }
}
