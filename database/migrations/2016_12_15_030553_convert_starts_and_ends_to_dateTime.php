<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertStartsAndEndsToDateTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
        {
         Schema::table('events', function ($table) {
            $table->dropColumn('starts');
            $table->dropColumn('ends');

        });   //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('events', function ($table) {
        $table->date('starts');
        $table->date('ends');

        });   
    }
}
