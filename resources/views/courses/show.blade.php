@extends('layouts.app')

@section('title', $course->title)

@section('content')

	<div class="container">
	    <h1>{{ $course->title }}</h1>
	    {!! $course->summary !!}
		{!! $course->content !!}
		<h2>Dates</h2>


		@foreach($events as $event)
			<h4>{{ $event['venue']['name'] }}</h4>
			<p>{{ $event->starts->format('l d') }} to {{ $event->ends->format('l d F Y') }} </p>
			<h4>£{{ $event->price }}</h4>
			<p><a href="/events/book/{{ $event->id }}" class="btn btn-primary" role="button">Book Now</a></p>
		@endforeach
	</div>

@endsection

@section('collections')
	<div class="container">
	<hr />
	<h4> Collections</h4>
		@foreach($course->tags as $tag)
			<p><a href="/courses/tags/{{ $tag['slug'] }}">{{ $tag['name'] }} courses</p>
		@endforeach
	</div>
@endsection





