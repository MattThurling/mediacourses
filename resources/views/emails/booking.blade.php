<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>{{ $subject or 'mediacourses.net booking' }}</title>
    <style type="text/css">
        #outlook a {padding:0;}
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        p {margin: 1em 0;}
        h1, h2, h3, h4, h5, h6 {color: black !important;}
        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {color: red !important;}
        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: purple !important; }
        a {color: #92ab2f;}
    </style>
</head>
<body>
<p> Dear {{ $first_name }}</p>
<p>Thank you! We have received your request for places on {{ $e->course['title'] }} in {{ $e->city }} on {{ $e->starts->format('l d F Y') }}.</p>
<h4>What happens now?</h4>
<p>The course operator will be in touch soon to confirm the details.</p>
<p>Name: {{ $first_name }} {{ $last_name }}<br/>
Course: {{ $e->course['title'] }}<br/>
Venue: {{ $e->venue['name'] }}<br/>
Date: {{ $e->starts->format('l d F Y') }}</p>
<p>The Bookings Team<br />
<a href="http://www.mediacourses.net">mediacourses.net</a></p>



</body>
</html>