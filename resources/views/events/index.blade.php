@extends('layouts.app')

@section('title', 'Events')

@section('content')
	<div class="container">
		@foreach ($events as $event)
			<h2>{{ ($event['course']['title']) }}</h2>
			<h4>{{ ($event['venue']['name']) }}</h4>
			<p>Starts: {{ $event->starts->format('l d F Y') }}</p>
			<p>Ends: {{ $event->ends->format('l d F Y') }}</p>
			<p><a href="/events/book/{{ $event->id }}" class="btn btn-primary" role="button">Book Now</a></p>
		@endforeach
	</div>
@endsection

