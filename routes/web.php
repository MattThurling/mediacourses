<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('mail', function ()
	{
		$data = [];
		Mail::send('emails.booking', $data, function($message)
		{
			$message->to('matt@cpdforteachers.com')
					->subject('It worked!');
		});
	});

Route::resource('courses', 'CourseController');
Route::get('courses/tags/{tag}', 'CourseController@showTagged');


Route::get('events', 'EventController@index');
Route::get('events/book/{id}', 'EventController@book');

Route::post('booking-requests', 'BookingRequestController@store');

Route::get('feed-events', 'FeedController@feedEvents');
Route::get('feed-courses', 'FeedController@feedCourses');
Route::get('tag-courses', 'FeedController@tagCourses');
