<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertStartsAndEndsToDateTimeStage2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
        {
         Schema::table('events', function ($table) {
            $table->dateTime('starts');
            $table->dateTime('ends');

        });   //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('events', function ($table) {
        $table->dropColumn('starts');
        $table->dropColumn('ends');

        });   
    }
}
