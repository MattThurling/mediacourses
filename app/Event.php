<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Money;

class Event extends Model
{
	protected $dates = ['starts','ends'];
 /**
	 * Venue where the event is held.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function venue()
	{
	    return $this->belongsTo(Venue::class);
	}

	/**
	 * Course being taught at this event.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function course()
	{
	    return $this->belongsTo(Course::class);
	}

	    /**
     * Get the price in pounds.
     *
     * @param  int  $value
     * @return float
     */
    public function getPriceAttribute($value)
    {
        return Money::fromPence($value)->inPounds();
    }

}
