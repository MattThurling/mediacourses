<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.head')
    </head>
    <body>
	    @include('layouts.nav')


	    @if (session()->has('flash_notification.message'))
			<div class="alert alert-{{ session('flash_notification.level') }}">
	    		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

	    			{!! session('flash_notification.message') !!}
			</div>
		@endif
		
	   	@yield('content')

	   	@yield('collections')
	   


	    @include('partials.scripts.analytics')
    </body>
</html>