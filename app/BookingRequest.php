<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRequest extends Model
{
    protected $fillable = [
    	'event_id',
		'first_name',
		'last_name',
		'email'
    ];
}
