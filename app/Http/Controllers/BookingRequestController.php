<?php

namespace App\Http\Controllers;

use Mail;
use App\Event;
use App\Course;
use App\BookingRequest;
use App\Http\Requests\CreateBookingRequest;

class BookingRequestController extends Controller
{
    public function store (CreateBookingRequest $request) 
    {
        BookingRequest::create($request->all());
        flash()->success('Thank you! Your booking request has been submitted.  You should receive an automated mail within the next few minutes.');

        // Send notification mail to office
        $e = Event::with('course','venue')->where('id', '=', $request->event_id)->first();
        $data = ['first_name' => $request->first_name,
                 'last_name' => $request->last_name,
                'e' => $e
                ];

        Mail::send('emails.booking', $data, function ($message) use ($request, $e) {
            $message->from('hello@mediacourses.net', 'mediacourses.net')
                    ->to($request->email)
                    ->cc('matt@cpdforteachers.com')
                    ->cc('jo@transmedia.co.uk')
                    ->subject('Booking request for ' . $e->course->title . ' in ' . $e->venue->name . ' on ' . $e->starts->format('d F Y'));
        });
        return redirect('courses');
    	

    }
}
